import { Address } from './../../../Models/address';
import { SimpleAccount } from './../../../Models/sipmleAccount';
import { AccountService } from './../../../Services/account/account.service';
import { ManagmentService } from './../../../Services/account/managment.service';
import { DataService } from './../../../Services/data.service';
import { Component, OnInit } from '@angular/core';
import { ComanyAccount } from "../../../Models/companyAccount";
import { Router, ActivatedRoute } from '@angular/router';
import { Ng2UploaderModule } from 'ng2-uploader';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  type="company";
  account:any;
  uploadFile: any;
  hasBaseDropZoneOver: boolean = false;
  image="default.png";
  options: Object = {
    url: 'http://localhost/upload/upload.php'
  };
  sizeLimit = 2000000;
 
  constructor(private ms:AccountService,private router:Router) { }

  ngOnInit() {
    if(this.ms.getuserIsConnected()==true)
    {
      this.router.navigate(['/profile']);
    }    
    
      }
  register(r){
   let address:Address;   
   address={gouvernorat:r.Governorat,municipalite:r.Municipalite,rue:r.Rue};
   let account:any;
   if(this.type=="simple")
   { 
   account = new SimpleAccount(r.about,true,null,false,new Date().toISOString(),r.email,null,r.login,r.password,r.phone,r.firstname,r.gender,r.lastname,this.image,address)
   account = {"Simple": account}; 
   }
   else
   {
     
    account = new ComanyAccount(r.about,true,null,false,new Date().toISOString(),r.email,null,r.login,r.password,r.phone,r.facebook,r.twitter,r.website,this.image,r.name,address)
    account = {"Company": account}; 
   }
   this.ms.register(account).subscribe(resp=>{
  
        this.ms.login(r.login,r.password).subscribe(resp=>{
        localStorage.setItem('token', resp.text());
        this.ms.setuserIsConnected(true);
        this.router.navigate(['/profile']);
    });
    
   });
  }
  

  handleUpload(data): void {
    if (data && data.response) {
      data = JSON.parse(data.response);
      
      this.uploadFile = data;
      this.image=this.uploadFile.generatedName;
    }
  }
 
  fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
 
  beforeUpload(uploadingFile): void {
    if (uploadingFile.size > this.sizeLimit) {
      uploadingFile.setAbort();
      alert('File is too large');
    }
  }
}
