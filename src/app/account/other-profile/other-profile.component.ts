import { CvService } from './../../Services/cv/cv.service';
import { Address } from './../../Models/address';
import { Account } from './../../Models/account';
import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../Services/account/account.service';
import { SimpleAccount } from '../../Models/sipmleAccount';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-other-profile',
  templateUrl: './other-profile.component.html',
  styleUrls: ['./other-profile.component.css']
})
export class OtherProfileComponent implements OnInit {
 account:any;
 type:any;
 private cvList:any[];
  constructor(private as:AccountService ,private address:Address,private router:Router,private cv:CvService ) { }
 
  ngOnInit() {
    if(this.as.getuserIsConnected()==false)
    {
      this.router.navigate(['/login']);
    }
    else{
    this.as.getProfileById().subscribe(resp=>{
    this.account=resp.json();
    if(this.account.Simple!=null)
    {
      this.type="Simple";
      this.account=this.account.Simple;
      this.address=this.account.address;
    }
    if(this.account.Company!=null)
    {
      this.type="Company";
      this.account=this.account.Company;
      this.address=this.account.address;
    }
    this.as.getCurrentUser().subscribe(resp=>{
      if(resp.json().Id==this.account.id)
      {

        this.router.navigate(['/profile']);
        
      }
    });
    this.cv.getCVofUser(this.account.id).subscribe(resp=>this.cvList=resp);
    });
   }
  }

}
