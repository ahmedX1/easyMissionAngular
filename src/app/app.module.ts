import { ManagmentService } from './Services/account/managment.service';
import { CvService } from './Services/cv/cv.service';
import { Address, FullAddress } from './Models/address';
import { HomeComponent } from './widgets/home/home.component';
import { Account, FullAccount } from './Models/account';
import { AccountService } from './Services/account/account.service';
import { DataService } from './Services/data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router'; 
import { AppComponent } from './app.component';
import { NavbarComponent } from "./widgets/navbar/navbar.component";
import { FooterComponent } from './widgets/footer/footer.component';
import { HeaderComponent } from './widgets/header/header.component';
import { LoginComponent } from "./account/login/login.component";
import {HttpModule} from '@angular/http'
import { FormsModule } from '@angular/forms';
import { ProfileComponent } from './account/profile/profile.component';
import { NgxPaginationModule} from 'ngx-pagination';
import { RegisterComponent } from './account/register/register/register.component';
import { VisitedProfileComponent } from './account/visited-profile/visited-profile.component';
import { Ng2UploaderModule } from 'ng2-uploader';
import { SearchComponent } from './account/search/search.component';
import { OtherProfileComponent } from './account/other-profile/other-profile.component';
import { TextEqualityValidatorModule } from "ngx-text-equality-validator";
import { FacebookModule } from 'ngx-facebook';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent,
    ProfileComponent,
    HomeComponent,
    RegisterComponent,
    VisitedProfileComponent,
    SearchComponent,
    OtherProfileComponent
    
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    NgxPaginationModule,
    Ng2UploaderModule,
    TextEqualityValidatorModule,
    FacebookModule.forRoot(),
    RouterModule.forRoot([
      
      {path:'login',component:LoginComponent},
      {path:'editProfile',component:VisitedProfileComponent},
      {path:'profile',component:ProfileComponent},
      {path:'home',component:HomeComponent},
      {path:'',component:HomeComponent},
      {path:'register',component:RegisterComponent},
      {path:'searchProfile',component:SearchComponent},
      {path:'visite',component:OtherProfileComponent},
      {path:'**',component:NavbarComponent}
    ])

  ],
  providers: [
    DataService,
    AccountService,
    Account,
    Address,
    CvService,
    ManagmentService,
    FullAccount,
    FullAddress
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
