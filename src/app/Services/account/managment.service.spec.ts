import { TestBed, inject } from '@angular/core/testing';

import { ManagmentService } from './managment.service';

describe('ManagmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManagmentService]
    });
  });

  it('should be created', inject([ManagmentService], (service: ManagmentService) => {
    expect(service).toBeTruthy();
  }));
});
