import { DataService } from './../data.service';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
@Injectable()
export class ManagmentService extends DataService{

   constructor(http: Http) {
      super('http://localhost:18080/easyMissionProject-web/v0/accounts',http);
  }

}
