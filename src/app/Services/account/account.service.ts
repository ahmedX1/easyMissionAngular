import { DataService } from './../data.service';
import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions } from '@angular/http';
import { ControlContainer } from '@angular/forms/src/directives/control_container';

@Injectable()
export class AccountService  {
headers = new Headers();
headers2= new Headers();
public visite=0;
//////
 body = JSON.stringify({ 'foo': 'bar' });
 header = new Headers({ 'Content-Type': 'application/json' });
 options = new RequestOptions({ headers: this.header });
//////
private userIsConnected=false;
   constructor(private http:Http) {
     if(localStorage.getItem('token')==null)
      this.userIsConnected=false;
     else
      this.userIsConnected=true;
      this.headers.append('Authorization',"Bearer "+localStorage.getItem('token'));
      this.headers2.append('Authorization', localStorage.getItem('token'));
  }
  
  login(login:string, pwd:string)
  {
     
     return this.http.post('http://localhost:18080/easyMissionProject-web/v0/accounts/login',{"login":login,"password":pwd});
     
  }
  updateProfile(account)
  {
     
     return this.http.put('http://localhost:18080/easyMissionProject-web/v0/accounts',JSON.stringify(account),this.options);
     
  }  
  register(account)
  {
     
     return this.http.post('http://localhost:18080/easyMissionProject-web/v0/accounts',account,this.options);
     
  }
  getCurrentUser()
  {  
    
    return this.http.get('http://localhost:53472/api/accounts/5',{headers:this.headers2});
  }     
  getProfileById()
  {
    
    return this.http.get('http://localhost:18080/easyMissionProject-web/v0/accounts/'+localStorage.getItem('visite'),{headers:this.headers});

  }
  getuserIsConnected():boolean
  {
   return this.userIsConnected;
  }
  setuserIsConnected(status:boolean)
  {
    this.userIsConnected=status;
  }
  getProfiles(searchedIteam){
    return this.http.get('http://localhost:18080/easyMissionProject-web/v0/accounts?search='+searchedIteam).map(Response=>Response.json());    
  
  }
  

}
