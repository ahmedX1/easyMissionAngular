import { DataService } from './../data.service';
import { Injectable } from '@angular/core';
import {Http} from '@angular/http'
import 'rxjs/add/operator/map';
@Injectable()
export class CvService {

  constructor(private http:Http) {
    
   }
  public getCVofUser(id) {
    return this.http.get('http://localhost:18080/easyMissionProject-web/v0/cvs/accounts/'+id).map(Response=>Response.json());
  }
}
