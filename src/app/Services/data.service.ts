import { Injectable } from '@angular/core';
import {Http} from '@angular/http'
import 'rxjs/add/operator/map';
@Injectable()
export class DataService {

 constructor(private url:string,private http: Http) {}

  public getAll() {
    return this.http.get(this.url).map(Response=>Response.json());
  }

  public create(ressource:any) {
   return this.http.post(this.url, JSON.stringify(ressource)).map(Response=>Response.json());
  }

  public update(ressource:any) {
    return this.http.put(this.url +'/'+ressource.id,ressource).map(Response=>Response.json());
  }
  public delete(ressource:any) {
    return this.http.delete(this.url +'/'+ressource.id).map(Response=>Response.json());
  }

}
