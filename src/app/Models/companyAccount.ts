import { Address } from './address';
export class ComanyAccount{
    
   Id: number;
   about: string;
   active: boolean;
   banneEnd: string;
   bannned: boolean;
   dateCreation: string;
   email: string;
   lastConnection : string;
   login: string;
   password: string;
   phone: number;
   facebook: string;
   twitter: string;
   website: string;
   logo: string;
   name:string;
   address:Address;
 constructor(About,Active,BanneEnd,Bannned,DateCreation,Email,LastConnection,Login,Password,Phone,facebook,twitter,website,logo,name,address){
  this.about=About;
  this.active=Active;
  this.banneEnd=BanneEnd;
  this.bannned=Bannned;
  this.dateCreation=DateCreation;
  this.email=Email;
  this.lastConnection=LastConnection;
  this.login=Login;
  this.password=Password;
  this.phone=Phone;
  this.facebook=facebook;
  this.twitter=twitter;
  this.website=website;
  this.logo=logo;
  this.name=name
  this.address=address;
 
 }

}