import { Address } from './address';
export class SimpleAccount{
    
   Id: number;
   about: string;
   active: boolean;
   banneEnd: string;
   bannned: boolean;
   dateCreation: string;
   email: string;
   lastConnection : string;
   login: string;
   password: string;
   phone: number;
   firstname: string;
   gender: string;
   lastname: string;
   photo: string;
   address:Address;
 constructor(About,Active,BanneEnd,Bannned,DateCreation,Email,LastConnection,Login,Password,Phone,firstname,gender,lastname,photo,address){
  this.about=About;
  this.active=Active;
  this.banneEnd=BanneEnd;
  this.bannned=Bannned;
  this.dateCreation=DateCreation;
  this.email=Email;
  this.lastConnection=LastConnection;
  this.login=Login;
  this.password=Password;
  this.phone=Phone;
  this.firstname=firstname;
  this.gender=gender;
  this.lastname=lastname;
  this.photo=photo;
  this.address=address;
 }

}