import { Address, FullAddress } from './address';
export class Account{
    
   DTYPE:string;
   Id: number;
   About: string;
   Active: boolean;
   BanneEnd: string;
   Bannned: boolean;
   DateCreation: string;
   Email: string;
   LastConnection : string;
   Login: string;
   Password: string;
   ConfirmPassword: string;
   Phone: number;
   Facebook: string;
   Logo: string;
   Name: string;
   Twitter: string;
   Website: string;
   firstname: string;
   gender: string;
   lastname: string;
   photo: number;
   address_Id: string;
   MyAddress: string;
    cvs : any[];
    topics: any[];
    stations: any[];
    offers: any[];
    address:Address;
    tokens: any[];

}

export class FullAccount{
    
   DTYPE:string;
   Id: number;
   About: string;
   Active: boolean;
   BanneEnd: string;
   Bannned: boolean;
   DateCreation: string;
   Email: string;
   LastConnection : string;
   Login: string;
   Password: string;
   ConfirmPassword: string;
   Phone: number;
   Facebook: string;
   Logo: string;
   Name: string;
   Twitter: string;
   Website: string;
   firstname: string;
   gender: string;
   lastname: string;
   photo: number;
   address_Id: string;
   MyAddress: string;
    cvs : any[];
    topics: any[];
    stations: any[];
    offers: any[];
    address:FullAddress;
    tokens: any[];

}